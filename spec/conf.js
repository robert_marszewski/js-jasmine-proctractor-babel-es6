exports.config = {
  framework: 'jasmine2',
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: ['../base/firstTry.js'],
  onPrepare: function () {
    browser.ignoreSynchronization = true;
    require("babel-register")({presets: ["es2015"], plugins: ["transform-async-to-generator"]});
    require("babel-polyfill");
  }
};
