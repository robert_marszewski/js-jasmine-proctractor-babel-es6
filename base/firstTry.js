const driver = require('selenium-webdriver');
describe('test', () => {
  it('should open google page', async () => {
    browser.get("http://www.google.pl");
    let header = $('#hplogo');
    expect(header).toBeTruthy();
    $('#lst-ib').sendKeys("google translate");
    $('#lst-ib').sendKeys(driver.Key.ENTER);
    $('#tw-source').click();
    browser.switchTo().activeElement().sendKeys("help");
    browser.sleep(500);
    expect($('#tw-target .tw-ta-container.tw-nfl pre span').getText()).toBe("Wsparcie");
  });
});
